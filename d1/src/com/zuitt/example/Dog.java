package com.zuitt.example;

public class Dog extends Animal {

    private String breed;

    public Dog()
    {
        super(); // Animal() constructor
        this.breed = "Great Pyrenees";
    }

    public Dog(String name, String color, String breed)
    {
        super(name, color); // Animal(String name, String color) -> constructor

        this.breed = breed;
    }

    // Getters
    public String getBreed()
    {
        return this.breed;
    }

    // Methods
    public void speak()
    {
        super.call(); // The method from Animal Class
        System.out.println("Ed... ward!\nBig Brother!");
    }
}
