package com.zuitt.example;

public interface Actions {

    // Interfaces - are used to achieve total abstraction.

    public void sleep();
    public void run();
}
