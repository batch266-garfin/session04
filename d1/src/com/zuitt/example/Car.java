package com.zuitt.example;

public class Car {

        // Object-Oriented Concepts

        // Here are the definitions of the following:

        // Object - An abstract idea in your mind that represents something in the real world
        // Example: The concept of a dog
        // Class - The representation of the object using code
        // Example: Writing code that would describe a dog
        // Instance - A unique copy of the idea, made "physical"

        // Object
        // Object are composed of two components

        // 1. States and Attributes
        // what is the idea about?
        // 2. Behaviours
        // What can idea do?

        // Ex: A person has attributes lik ename, age, height, weight. And a person can eat, sleep, speak.

        // Class Creation
        // A class is composed of four parts:
        // 1. Properties - characteristics of the object.
        // 2. Constructors - used to create an objects
        // 3. Getters/ Setters - get and set the values of each property of the object
        // Methods - functions that an object can perform

        // Properties
        // public, private, protected, default
        private String name;
        private String brand;
        private int yearOfMake;

        // Constructors

        // Empty Constructor
        //private Car(){}

        // Parameterized Constructors
        private Car(String name, String brand, int yearOfMake)
        {
            this.name = name;
            this.brand = brand;
            this.yearOfMake = yearOfMake;
        }

        // Getters and Setters

        // Setters
        public void setName(String name)
        {
            this.name = name;
        }

        public void setBrand(String brand)
        {
            this.brand = brand;
        }

        public void setYearOfMAke(int yearOfMake)
        {
            this.yearOfMake = yearOfMake;
        }

        // Getters

        public String getName()
        {
            return this.name;
        }

        public String getBrand()
        {
            return this.brand;
        }

        public int getYearOfMake()
        {
            return this.yearOfMake;
        }

        // Methods

        public void drive()
        {
            System.out.println("The car is running.");
        }

        public void stop()
        {
            System.out.println("The car stopped.");
        }

        // Access Modifiers
        // 1. Default - no keyword required
        // 2. Private - only accessible within the class
        // 3. Protected - only accessible to / within the classes (same package)
        // 4. Public - can be accessed anywhere

        // Fundamentals of OOP
        // 1. Encapsulation - mechanism that wrapping data or variables and code acting on the data
        // 2. Inheritance - properties can be shared to subclasses
        // 3. Abstraction - way of using functions, process, codes without knowing its origin.
        // 4. Polymorphism - Passing of attribute and can be added or modified.

        // Make Driver a component of Car

        private Driver d;

        public Car()
        {
            this.d = new Driver("Alejandro");
        }

        public String getDriverName()
        {
            return this.d.getName();
        }


}
