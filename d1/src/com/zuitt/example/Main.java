package com.zuitt.example;

public class Main {

    public static void main(String[] args) {

        Car myCar = new Car();

        System.out.println("This car is driven by " + myCar.getDriverName());

//        Dog myPet = new Dog();
//        myPet.setName("Brownie");
//        myPet.setColor("Brown");
//        myPet.speak();
//
//        System.out.println(myPet.getName() + " " + myPet.getBreed() + " " + myPet.getColor());

        Dog myDreamPet = new Dog();
        myDreamPet.setName("Alexander");
        myDreamPet.setColor("White");
        myDreamPet.speak();

        System.out.println("My name is " + myDreamPet.getName());
        System.out.println("My color is " + myDreamPet.getColor());

        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMAke(2025);

        System.out.println("Car name: " + myCar.getName());
        System.out.println("Car brand: " + myCar.getBrand());
        System.out.println("Car year of make: " + myCar.getYearOfMake());
        System.out.println("Car driver: " + myCar.getDriverName());


        // Abstraction
        // is a process where all the logic and complexity are hidden from the user.


        Person child = new Person();

        child.sleep();
        child.run();

        // Polymorphism
        // Delivered from the greek word: poly means many  and morph means forms

        StaticPoly myAddition = new StaticPoly();

        System.out.println(myAddition.addition(3,4));
        System.out.println(myAddition.addition(2,4,7));
        System.out.println(myAddition.addition(3.4,3.5));
    }
}
