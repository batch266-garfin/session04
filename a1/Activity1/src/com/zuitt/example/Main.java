package com.zuitt.example;

public class Main {

    public static void main(String[] args) {

        PhoneBook phonebook = new PhoneBook();

//        phonebook.setName("aaa");
//        phonebook.setContactNumber("0909");
//        phonebook.setAddress("BBB");
//
//        phonebook.setName("ccc");
//        phonebook.setContactNumber("0808");
//        phonebook.setAddress("DDD");

//        System.out.println(phonebook.getName());
//        System.out.println(phonebook.getContactNumber());
//        System.out.println(phonebook.getAddress());

        Contact contact1 = new Contact("Rom","0909", "Hell");
        Contact contact2 = new Contact("Mhel","0808", "Heaven");

        phonebook.setContact(contact1);
        phonebook.setContact(contact2);

        if(phonebook.getContact().isEmpty())
        {
            System.out.println("The phonebook is currently empty.");
        }
        else
        {
            phonebook.getContact().forEach((contact) ->
            {
                displayContact(contact);
            });
        }
    }
    public static void displayContact(Contact contact){
        System.out.println(contact.getName());
        System.out.println(contact.getContactNumber());
        System.out.println(contact.getAddress());
        System.out.println("-------------------");
    }
}
