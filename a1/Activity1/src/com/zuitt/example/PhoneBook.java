package com.zuitt.example;

import java.util.ArrayList;

public class PhoneBook extends Contact {

    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public PhoneBook()
    {

    }

    public PhoneBook(Contact contact)
    {
        this.contacts.add(contact);
    }

    public void setContact(Contact contact)
    {
        contacts.add(contact);
    }

    public ArrayList<Contact> getContact()
    {
        return contacts;
    }

}
